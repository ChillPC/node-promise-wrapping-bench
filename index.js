const Benchmark = require('benchmark');

function returnPromise () {
  return new Promise((resolve, reject) => resolve("My promise"));
}

function returnNonPromise () {
  return "My non promise";
}

function wrapInPromise (func) {
  Promise.resolve(func)
    .then(
      (res) => {
        return res;
      },
      (error) => {
        console.log("error: ", error);
      }
    );
}

function testIfPromise (func) {
  if (typeof promise === 'object' && typeof promise.then === 'function') {
    func()
      .then(
        (res) => {
          return res;
        },
        (error) => {
          console.log("error: ", error);
        }
      );
  }
}

const n = 5;

new Benchmark.Suite()
  .add('Test with promise', async () =>
    testIfPromise(returnPromise),
    {'minSamples': n}
  )
  .add('Test with non promise', async () =>
    testIfPromise(returnNonPromise),
    {'minSamples': n}
  )
  .add('Wrap with promise', async () =>
    wrapInPromise(returnPromise),
    {'minSamples': n}
  )
  .add('Wrap with non promise', async () =>
    wrapInPromise(returnNonPromise),
    {'minSamples': n}
  )
  .on('cycle', function(event) {
    console.log(String(event.target));
  })
  .on('complete', function () {
    this.forEach((b) => {
      console.log("----- ", b.name, '\n', b.times);
    });
  })
  .run({
    'async': true,
  });
